import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfter, FunSuite}
import schema.PetaSkySchema

class StatsTest extends FunSuite with BeforeAndAfter {
  var sc: SparkContext = null;

  // exécuté avant chaque test
  before {
    // configuration de Spark
    val conf = new SparkConf()
      .setAppName("StatsTest")
      .setMaster("local") // ici on configure un faux cluster spark local pour le test
    sc = new SparkContext(conf)
  }
  def checkResult(result: Array[Double]): Any = {
    val ecartTypes = result.zip(PetaSkySchema.s_dataTuple)
    ecartTypes.foreach(r => println(">>> %3d - %25s : ".format(r._2._2, r._2._1) + r._1))

    assert((ecartTypes.find(_._2._1 == "ra").get._1 - 0.315858).abs < 0.000001)
    assert((ecartTypes.find(_._2._1 == "raSigmaForDetection").get._1 - 0.362813).abs < 0.000001)
    assert((ecartTypes.find(_._2._1 == "raSigmaForWcs").get._1 - 0).abs < 0.000001)
    assert(ecartTypes.find(_._2._1 == "xFlux").get._1.isNaN)
    assert((ecartTypes.find(_._2._1 == "xAstromSigma").get._1 - 0.421395).abs < 0.000001)
  }

  test("Vérifie les écart-types moyens calculés") {
    val res = Stats.computeMeanStdByObject("samples/source-sample", sc)
    checkResult(res)
  }

  // exécuté après chaque test
  after {
    sc.stop() // a ne pas oublier, car il ne peut pas y avoir 2 contextes spark locaux simultanément
  }
}
