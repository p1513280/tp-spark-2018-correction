import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfter, FunSuite}
import schema.PetaSkySchema

class PartitionTest extends FunSuite with BeforeAndAfter {
  var sc: SparkContext = null;

  // exécuté avant chaque test
  before {
    // configuration de Spark
    val conf = new SparkConf()
      .setAppName("PartitionTest")
      .setMaster("local") // ici on configure un faux cluster spark local pour le test
    sc = new SparkContext(conf)
  }

  test("Vérifie le partitionnement") {
    val sizeInO = 11 * 1024

    val (parts1, nbCases1) = Partition.partition("samples/test/", sc, 1, sizeInO)
    val (parts2, nbCases2) = Partition.partition("samples/test/", sc, 2, sizeInO)

    val (nbPlusieurs1, nbDepasses1) = Partition.qualitePartitionnement(parts1, sizeInO)
    val (nbPlusieurs2, nbDepasses2) = Partition.qualitePartitionnement(parts2, sizeInO)
    println(s"nbPlusieurs1 = $nbPlusieurs1")
    println(s"nbPlusieurs2 = $nbPlusieurs2")
    println(s"nbDepasses1 = $nbDepasses1")
    println(s"nbDepasses2 = $nbDepasses2")

    assert(nbCases1 == 4)
    assert(nbCases2 == 4)
    assert(nbPlusieurs1 == 0)
    assert(nbPlusieurs2 == 0)
    assert(nbDepasses1 == 1)
    assert(nbDepasses2 == 2)
  }

  // exécuté après chaque test
  after {
    sc.stop() // a ne pas oublier, car il ne peut pas y avoir 2 contextes spark locaux simultanément
  }
}