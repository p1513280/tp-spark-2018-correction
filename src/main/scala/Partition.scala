import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.hadoop.fs.{Path, FileSystem}
import org.apache.hadoop.conf.Configuration
import schema.PetaSkySchema
import Math.{min, max, E, log, ceil, pow}
import Utils.noNaN

object Partition {
  val compte = "p1513280"

  def nthRoot(n:Double, x:Double): Double = {
    Math.pow(Math.E, Math.log(x)/n)
  } 

  // n est le nombre d'attributs sur lequel partitionner et sizeInO est la taille moyenne voulue d'une partition en octets
  // renvoie un RDD contenant des paires (id de partition, ligne à écrire) et l'estimation du nombre de cases à écrire
  def partition(inputDir: String, sc: SparkContext, n: Int, sizeInO: Long):
  (RDD[(String,String)], Int) = {
    // On calcule le nombre de partitions de chaque attribut
    val sizeOfDir = Utils.getHdfsDirSize(sc, inputDir)
    // on utilise la fonction plafond car on préfère trop partitionner que pas assez (pour éviter de dépasser 128Mo)
    val k = ceil(nthRoot(n, sizeOfDir/sizeInO)).toInt
    val nbCases = pow(k,n).toInt

    val ecartTypes = Stats.computeMeanStdByObject(inputDir,sc).zip(PetaSkySchema.s_dataTuple)

    // Les n meilleurs champs à garder avec leur écart-type, leur min, leur max, l'indice et le nom du champ
    var nBest = ecartTypes.filter( { case (std,_) => !std.isNaN && std.abs > 0.000001 })
                          .sortWith( { case ((std1,_), (std2,_)) => std1 < std2 })
                          .take(n)

    // Computing min and max value for those attributes
    val nBestFinal = sc.textFile(inputDir)
                      .map(_.split(","))
                      .flatMap(source => for (t <- nBest; i = t._2._2; str = source(i);
                                              if (str != "NULL"); value = str.toDouble) yield (i, (value, value))
                      )
                      .reduceByKey({ case ((min1, max1), (min2, max2)) =>
                        (min(min1, min2), max(max1, max2))
                      })
                      .map({ case (i, (mini, maxi)) => {
                        val partSize = (maxi - mini) / k
                        (i, mini, partSize)
                      }}).collect()

    println("nBestFinal : ")
    nBestFinal.foreach(println)

    val r = scala.util.Random

    (sc.textFile(inputDir)
      .map(source => {
        val sp = source.split(",")
        var key = "part"
        val lengthOfK = k.toString.length
        val formatString = s"_%0${lengthOfK}d"
        nBestFinal.foreach({
          case (i, mini, partSize) => {
            if (sp(i) != "NULL") {
              val attrVal = sp(i).toDouble
              var part = ((attrVal - mini) / partSize).toInt
              // si on dépasse k - 1 à cause d'un problème de précision de double
              if (part == k)
                part = k - 1
              key += formatString.format(part)
            } else 
              key += formatString.format(r.nextInt(k))
         }
        })
        (key, source)
      }), nbCases)
  }

  // Renvoie nbPlusieurs le nombre d'objets dans plusieurs partitions et
  // nbDepasses le nombre de partitions qui ont dépassé la taille voulue
  def qualitePartitionnement(parts: RDD[(String, String)], sizeInO: Long): (Long, Long) = {
    val nbPlusieurs = parts.map({ case (part, source) => (source.split(",")(PetaSkySchema.s_objectId), part)})
                           .filter({ case (oId, part) => !oId.equals("NULL") })
                           .reduceByKey({ case (part1, part2) => if (part1.equals(part2)) part1 else "OUT" })
                           .filter({ case (oId, part) => part.equals("OUT") })
                           .count()
    // Dans l'encodage utilisé, 1 caractère vaut 1 octet, et on oublie pas le saut de ligne qui fait également 1 octet
    val nbDepasses = parts.map({ case (part, source) => (part, source.length + 1L) })
                          .reduceByKey((a:Long,b:Long) => a + b)
                          .filter({ case (part, size:Long) => size > sizeInO })
                          .count()
    (nbPlusieurs, nbDepasses)
  }

  def main(args: Array[String]): Unit = {
    if (args.length >= 3) {
      val conf = new SparkConf().setAppName("Partition-" + compte)
      val sc = new SparkContext(conf)
      val sizeInO = 128 * 1024 * 1024

      val (parts, nbCases) = partition(args(1), sc, args(0).toInt, sizeInO)
      TIW6RDDUtils.writePairRDDToHadoopUsingKeyAsFileName(parts, args(2), nbCases)

      val (nbPlusieurs, nbDepasses) = qualitePartitionnement(parts, sizeInO)
      println(s"nbPlusieurs = $nbPlusieurs")
      println(s"nbDepasses = $nbDepasses")
    } else {
      println("Usage: spark-submit --class Partition /home/" + compte + "/SparkTP2App-assembly-1.0.jar " +
        "numberOfAttributesToUseForPartionning" +
        "hdfs:///user/" + compte + "/repertoire-donnees " +
        "hdfs:///user/" + compte + "/repertoire-partionnement")
    }
  }
}