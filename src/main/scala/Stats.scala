import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import schema.PetaSkySchema
import Math.{min, max, sqrt}
import Utils.noNaN

object Stats {
  val compte = "p1513280"

  case class StatsAgg(count: Long, mini: Double,
                      maxi: Double, sum: Double,
                      sumSquared: Double) {
    def agg(b: StatsAgg): StatsAgg = {
      StatsAgg(
        count + b.count,
        noNaN(min, mini, b.mini),
        noNaN(max, maxi, b.maxi),
        sum + b.sum,
        sumSquared + b.sumSquared
      )
    }

    def computeStd(): Double = {
      // Si on a aucune valeur non nulle pour l'attribut pour cet objet, l'écart-type n'est pas défini
      if (count == 0)
        Double.NaN
      // Si on a toujours la même valeur alors l'écart-type est nul
      else if (maxi.equals(mini))
        0
      else {
        val mean = sum / count
        val meanSquared = mean * mean
        sqrt((sumSquared / count - meanSquared)) / (maxi - mini)
      }
    }
  }

  def toStatsAgg(input: String): StatsAgg = {
    val isNull = input == "NULL"
    val init = if (isNull) Double.NaN else input.toDouble
    val sum = if (isNull) 0 else init
    StatsAgg(if (isNull) 0L else 1L, init, init, sum, sum * sum)
  }

  def computeMeanStdByObject(inputDir: String, sc: SparkContext):
  Array[Double] = {
    sc.textFile(inputDir)
      .map(_.split(","))
      .map(source => (source(PetaSkySchema.s_objectId),
                      PetaSkySchema.s_data.map(i => source(i))))
      .filter({ case (oId, source) => oId != "NULL"})
      .map({ case (oId, source) => (oId, source.map(toStatsAgg)) })
      // On calcule les écart-types en aggrégeant les StatsAgg de chaque attribut
      // (String, Array[Double]) : Object_Id -> Tableau des écart-types pour chaque attribut
      .reduceByKey((aggArray1, aggArray2) => aggArray1.zip(aggArray2).map({ case (agg1, agg2) => agg1.agg(agg2) }))
      .map({ case (oId, aggArray) => aggArray.map( agg => {
        val std = agg.computeStd
        val isNaN = std.isNaN
        (if (isNaN) 0 else std, if (isNaN) 0L else 1L)
      })})
      // On calcule les écart-types moyens
      // ce reduce produit un Array[Double] qui est notre tableau d'écarts-types moyens
      .reduce((aggArray1, aggArray2) => aggArray1.zip(aggArray2).map({
        case ((std1, count1), (std2, count2)) => {
          (std1 + std2, count1 + count2)
          }}))
      .map({ case (sumStd, count) => sumStd / count })
  }

  def main(args: Array[String]): Unit = {
    if (args.length >= 2) {
      val conf = new SparkConf().setAppName("Stats-" + compte)
      val sc = new SparkContext(conf)
      val result = computeMeanStdByObject(args(0), sc)
      sc.parallelize(
        result
          .zip(PetaSkySchema.s_dataTuple)
          .map(r => ">>> %3d - %25s : ".format(r._2._2, r._2._1) + r._1),
        1
      ).saveAsTextFile(args(1))
    } else {
      println("Usage: spark-submit --class Stats /home/" + compte + "/SparkTP2App-assembly-1.0.jar " +
        "hdfs:///user/" + compte + "/repertoire-donnees " +
        "hdfs:///user/" + compte + "/repertoire-resultat")
    }
  }
}