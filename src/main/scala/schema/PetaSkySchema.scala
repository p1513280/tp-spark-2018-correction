package schema

object PetaSkySchema {

  val sourceS = Utils.loadSchema(getClass.getResource("/Source.sql"))
  val objectS = Utils.loadSchema(getClass.getResource("/Object.sql"))

  val s_objectId = sourceS.indexOf("objectId")
  val s_sourceId = sourceS.indexOf("sourceId")
  val s_ra = sourceS.indexOf("ra")
  val s_decl = sourceS.indexOf("decl")

// Attributs à garder pour le calcul d'écart-type
  val s_dataTuple = sourceS.zipWithIndex.filter({ case (att,i) => {
    val notContainsId = att.indexOf("Id") == -1
    val notContainsObject = att.indexOf("Object") == -1
    val notContainsFlag = att.toLowerCase.indexOf("flag") == -1
    val notExtendedness = att.indexOf("extendedness") == -1
    notContainsId && notContainsObject && notContainsFlag && notExtendedness
  }})
  val s_dataName = s_dataTuple.unzip._1
  val s_data = s_dataTuple.unzip._2
}