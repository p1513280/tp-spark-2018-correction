import org.apache.hadoop.fs.{Path, FileSystem}
import org.apache.spark.SparkContext
import org.apache.hadoop.conf.Configuration

object Utils {
  // if one of the operand is NaN, returns the other, otherwise computes f(a,b)
  def noNaN(f: (Double,Double) => Double, a: Double, b: Double): Double = {
    if (a.isNaN) b else if (b.isNaN) a else f(a,b)
  }

  def getHdfsDirSize(sc: SparkContext, dir: String): Double = {
    FileSystem.get( sc.hadoopConfiguration ).listStatus( new Path(dir)).map(_.getLen).sum
  }
}
