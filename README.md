# TP2 Spark 2019

## Auteurs

* Jonathan Cabezas p1513280
* Que Lien Bui p1419782

## Calcul des statistiques sur les attributs

### Choix

* Nous avons choisi de garder les attributs en ligne plutôt que de les séparer en triplet pensant gagner en performances. Nous remettrons en cause ce choix par le suite.

* Nous avons omis les attributs contenant "Id", "Object", "flag" ou "Flag" et l'attribut "extendedness" ne nous laissant que les valeurs flottantes.

* Nous n'avons pas mis d'extrait de code dans notre rapport. Nos fonctions sont assez concises et commentées et se trouvent dans Stats.scala et Partition.scala.

* Nous avons implémenté des tests dans StatsTest.scala et PartitionTests.scala.

### Robustesse

Notre algorithme est robuste: 
 * Une valeur outlier trop grande pour rentrer dans un Double provoquerait un overflow, dont l'appel à .toDouble créerait un Double.Infinity, le calcul d'écart-type finirait donc par calculer Infinity - Infinity pour cet objet, résultant en un Double.NaN qui serait ignorés pour la moyenne des écarts-types
 * Les attributs n'ayant pas de valeur ("NULL") sont convertis en Double.NaN et ignorés lors de l'opération reduce.

### Sortie

```
$ spark-submit --class Stats /home/p1513280/SparkTP2App-assembly-1.0.jar hdfs:///petasky/Source hdfs:///user/p1513280/stats
```

Voici la sortie de notre algorithme sur Source, de la forme (indexAttribut - nomAttribut : Ecart-type moyen)

*   6 -                        ra : 0.24195035647777763
*   7 -       raSigmaForDetection : 0.23452129465147306
*   8 -             raSigmaForWcs : 0.0
*   9 -                      decl : 0.24187899062283305
*  10 -     declSigmaForDetection : 0.2346979824644317
*  11 -           declSigmaForWcs : 0.0
*  13 -                     xFlux : NaN
*  14 -                xFluxSigma : NaN
*  15 -                     yFlux : NaN
*  16 -                yFluxSigma : NaN
*  17 -                    raFlux : 0.29976688473991275
*  18 -               raFluxSigma : 0.0
*  19 -                  declFlux : 0.28948568460430574
*  20 -             declFluxSigma : 0.0
*  21 -                     xPeak : NaN
*  22 -                     yPeak : NaN
*  23 -                    raPeak : 0.29976688473991275
*  24 -                  declPeak : 0.28948568460430574
*  25 -                   xAstrom : 0.311153034845881
*  26 -              xAstromSigma : 0.24058830115596788
*  27 -                   yAstrom : 0.3112309213140794
*  28 -              yAstromSigma : 0.24065748446829258
*  29 -                  raAstrom : 0.24195035647777763
*  30 -             raAstromSigma : 0.23452129465147306
*  31 -                declAstrom : 0.24187899062283305
*  32 -           declAstromSigma : 0.2346979824644317
*  35 -               taiMidPoint : 0.3067003887379831
*  36 -                  taiRange : 0.0
*  37 -                   psfFlux : 0.26343637362845634
*  38 -              psfFluxSigma : 0.2512481154032687
*  39 -                    apFlux : 0.2633878217657188
*  40 -               apFluxSigma : 0.24789887421382292
*  41 -                 modelFlux : NaN
*  42 -            modelFluxSigma : NaN
*  43 -                 petroFlux : NaN
*  44 -            petroFluxSigma : NaN
*  45 -                  instFlux : NaN
*  46 -             instFluxSigma : NaN
*  47 -           nonGrayCorrFlux : NaN
*  48 -      nonGrayCorrFluxSigma : NaN
*  49 -               atmCorrFlux : NaN
*  50 -          atmCorrFluxSigma : NaN
*  51 -                     apDia : 0.2399821500018835
*  52 -                       Ixx : 0.23551522059128246
*  53 -                  IxxSigma : 0.25511441488890474
*  54 -                       Iyy : 0.2357701254676453
*  55 -                  IyySigma : 0.2565362767572569
*  56 -                       Ixy : 0.21841518403251514
*  57 -                  IxySigma : 0.25541669289084923
*  58 -                    psfIxx : NaN
*  59 -               psfIxxSigma : NaN
*  60 -                    psfIyy : NaN
*  61 -               psfIyySigma : NaN
*  62 -                    psfIxy : NaN
*  63 -               psfIxySigma : NaN
*  64 -                     e1_SG : NaN
*  65 -               e1_SG_Sigma : NaN
*  66 -                     e2_SG : NaN
*  67 -               e2_SG_Sigma : NaN
*  68 -             resolution_SG : NaN
*  69 -                 shear1_SG : NaN
*  70 -           shear1_SG_Sigma : NaN
*  71 -                 shear2_SG : NaN
*  72 -           shear2_SG_Sigma : NaN
*  73 -            sourceWidth_SG : NaN
*  74 -      sourceWidth_SG_Sigma : NaN
*  76 -                       snr : 0.0
*  77 -                      chi2 : 0.0
*  78 -                       sky : NaN
*  79 -                  skySigma : NaN
*  81 -             flux_Gaussian : 0.2503504187766842
*  82 -       flux_Gaussian_Sigma : 0.2591423011447319
*  83 -                  flux_ESG : 0.2420307857529903
*  84 -            flux_ESG_Sigma : 0.23854317356124752
*  85 -                sersicN_SG : NaN
*  86 -          sersicN_SG_Sigma : NaN
*  87 -                 radius_SG : NaN
*  88 -           radius_SG_Sigma : NaN
*  89 -          flux_flux_SG_Cov : NaN
*  90 -            flux_e1_SG_Cov : NaN
*  91 -            flux_e2_SG_Cov : NaN
*  92 -        flux_radius_SG_Cov : NaN
*  93 -       flux_sersicN_SG_Cov : NaN
*  94 -              e1_e1_SG_Cov : NaN
*  95 -              e1_e2_SG_Cov : NaN
*  96 -          e1_radius_SG_Cov : NaN
*  97 -         e1_sersicN_SG_Cov : NaN
*  98 -              e2_e2_SG_Cov : NaN
*  99 -          e2_radius_SG_Cov : NaN
* 100 -         e2_sersicN_SG_Cov : NaN
* 101 -      radius_radius_SG_Cov : NaN
* 102 -     radius_sersicN_SG_Cov : NaN
* 103 -    sersicN_sersicN_SG_Cov : NaN

### Ecart-types nuls

Il semblerait que les attributs raSigmaForWcs, declSigmaForWcs, raFluxSigma, declFluxSigma, taiRange, snr, chi2 aient un écart-type moyen de 0.
Cela peut avoir 2 explications : 
 * Soit l'attribut est extrêmement lié à l'objet identique pour toutes les sources liées à l'objet) et varie d'un objet à l'autre. C'est alors un très bon moyen d'identifier les objets concernés par une source.
 * Soit l'attribut est le même pour tous les objets et c'est alors un très mauvais moyen d'identifier les objets concernés par une source.

On vérifie dans quel cas nous nous trouvons:

```
$ hdfs dfs -cat /petasky/Source/Source-001.csv | head -20 | cut -d"," -f"1 9 12 37 77 78"

29710725217517768,0,0,15,0,0
29710725217517996,0,0,15,0,0
29710725217518033,0,0,15,0,0
29710725217518458,0,0,15,0,0
29710780246786437,0,0,15,0,0
29710780246786874,0,0,15,0,0
29710780246787547,0,0,15,0,0
29710780246787965,0,0,15,0,0
29710796017369469,0,0,15,0,0
29710796017369818,0,0,15,0,0
29710796017370366,0,0,15,0,0
29710796017370600,0,0,15,0,0
29710796017370723,0,0,15,0,0
29710796017370737,0,0,15,0,0
29710796017370963,0,0,15,0,0
29710854737625288,0,0,15,0,0
29710854737626265,0,0,15,0,0
29710854737626421,0,0,15,0,0
29710854737626460,0,0,15,0,0
29710854737627123,0,0,15,0,0
```

Il semblerait que tous nos attributs à 0 d'écart-type sont en fait identiques pour tous les objets. On prendra donc les plus petits écart-types en écartant les valeurs nulles pour les partitionnement.

### Ecart-types non définis (NaN)

Les écarts-types non définis sont obtenus pour les attributs étant toujours à NULL.

### Performances

Notre algorithme de calcul des écart-types renvoient un résultat en environ 60s sur le jeu de données Source de Petasky. D'autres groupes avec qui nous avons discuté rapportaient des temps de l'ordre de 30 secondes en utilisant des triplets (objectId, attribut, valeur).

Il semblerait en effet qu'il soit plus efficace de séparer de la sorte puisque cela permet de se débarasser des valeurs "NULL" plutôt que de devoir faire avec dans le reduce, causant des comparaisons supplémentaires à tout va.

## Partitionnement des données

### Choix

* Comme précisé plus haut nous avons omis les écart-types nuls car ce sont des données identiques pour tous les objets.
* Pour les sources ayant comme valeur "NULL" sur un attribut choisi pour le partitionnement, nous choisissons la coordonnée pour cet attribut dans l'hyperplan au hasard.
* Les fichiers sont nommés part_index1_index2..._indexN avec indexI étant l'intervalle choisit pour le ième attribut.

### Qualité du partitionnement sur 1 attribut

* nbPlusieurs = 199238
* nbDepasses = 1

nbPlusieurs représente le nombre d'objets étant dans plusieurs partitions et nbDepasses le nombre de partitions dépassant 128Mo.

Affichons une partie de ce partitionnement : 

```
$ hdfs dfs -ls -h /user/p1513280/part1
Found 187 items
-rw-r--r--   3 p1513280 p1513280           0 2019-11-18 23:20 /user/p1513280/part1/_SUCCESS
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_000
-rw-r--r--   3 p1513280 p1513280       1.8 M 2019-11-18 23:18 /user/p1513280/part1/part_001
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_002
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_003
-rw-r--r--   3 p1513280 p1513280       1.8 M 2019-11-18 23:18 /user/p1513280/part1/part_004
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_005
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_006
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_007
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_008
-rw-r--r--   3 p1513280 p1513280       1.8 M 2019-11-18 23:18 /user/p1513280/part1/part_009
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_010
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_011
-rw-r--r--   3 p1513280 p1513280       1.8 M 2019-11-18 23:18 /user/p1513280/part1/part_012
-rw-r--r--   3 p1513280 p1513280       1.8 M 2019-11-18 23:18 /user/p1513280/part1/part_013
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_014
-rw-r--r--   3 p1513280 p1513280       2.0 M 2019-11-18 23:18 /user/p1513280/part1/part_015
-rw-r--r--   3 p1513280 p1513280      22.8 G 2019-11-18 23:20 /user/p1513280/part1/part_016
-rw-r--r--   3 p1513280 p1513280       2.4 M 2019-11-18 23:18 /user/p1513280/part1/part_017
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_018
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_019
-rw-r--r--   3 p1513280 p1513280       1.9 M 2019-11-18 23:18 /user/p1513280/part1/part_020
```

On remarque qu'une seule partition contient l'immense majorité des sources. Les autres partitions ont à peu près la même taille, ce sont possiblement les valeurs "NULL" partitionnées au hasard.

Une des raisons possibles serait que des valeurs extrêmes augmentent la taille des intervalles, causant la plupart des observations à tomber dans le même intervalle. On pourrait essayer d'écarter les valeurs extrêmes pour améliorer les performances du partitionnement.

### Qualité du partitionnement sur 3 attributs

* nbPlusieurs = 173794
* nbDepasses = 1

```
$ hdfs dfs -ls -h /user/p1513280/part3
Found 23 items
-rw-r--r--   3 p1513280 p1513280           0 2019-11-18 23:29 /user/p1513280/part3/_SUCCESS
-rw-r--r--   3 p1513280 p1513280      22.9 G 2019-11-18 23:29 /user/p1513280/part3/part_0_0_0
-rw-r--r--   3 p1513280 p1513280      57.8 M 2019-11-18 23:26 /user/p1513280/part3/part_0_0_1
-rw-r--r--   3 p1513280 p1513280      57.8 M 2019-11-18 23:26 /user/p1513280/part3/part_0_0_2
-rw-r--r--   3 p1513280 p1513280      58.0 M 2019-11-18 23:26 /user/p1513280/part3/part_0_0_3
-rw-r--r--   3 p1513280 p1513280      57.5 M 2019-11-18 23:26 /user/p1513280/part3/part_0_0_4
-rw-r--r--   3 p1513280 p1513280      57.7 M 2019-11-18 23:26 /user/p1513280/part3/part_0_0_5
-rw-r--r--   3 p1513280 p1513280      12.7 K 2019-11-18 23:26 /user/p1513280/part3/part_1_1_0
-rw-r--r--   3 p1513280 p1513280       1.5 K 2019-11-18 23:26 /user/p1513280/part3/part_1_1_1
-rw-r--r--   3 p1513280 p1513280         750 2019-11-18 23:26 /user/p1513280/part3/part_1_1_2
-rw-r--r--   3 p1513280 p1513280       1.5 K 2019-11-18 23:26 /user/p1513280/part3/part_1_1_3
-rw-r--r--   3 p1513280 p1513280         749 2019-11-18 23:26 /user/p1513280/part3/part_1_1_4
-rw-r--r--   3 p1513280 p1513280       1.5 K 2019-11-18 23:26 /user/p1513280/part3/part_1_1_5
-rw-r--r--   3 p1513280 p1513280       3.0 K 2019-11-18 23:26 /user/p1513280/part3/part_2_2_0
-rw-r--r--   3 p1513280 p1513280         752 2019-11-18 23:26 /user/p1513280/part3/part_2_2_1
-rw-r--r--   3 p1513280 p1513280         765 2019-11-18 23:26 /user/p1513280/part3/part_2_2_2
-rw-r--r--   3 p1513280 p1513280       1.5 K 2019-11-18 23:26 /user/p1513280/part3/part_2_2_4
-rw-r--r--   3 p1513280 p1513280       1.5 K 2019-11-18 23:26 /user/p1513280/part3/part_2_2_5
-rw-r--r--   3 p1513280 p1513280         770 2019-11-18 23:26 /user/p1513280/part3/part_3_3_0
-rw-r--r--   3 p1513280 p1513280         763 2019-11-18 23:26 /user/p1513280/part3/part_4_4_0
-rw-r--r--   3 p1513280 p1513280       1.4 K 2019-11-18 23:26 /user/p1513280/part3/part_4_4_1
-rw-r--r--   3 p1513280 p1513280         754 2019-11-18 23:26 /user/p1513280/part3/part_5_5_1
-rw-r--r--   3 p1513280 p1513280         758 2019-11-18 23:26 /user/p1513280/part3/part_5_5_5
```

Encore la même observation. Cependant on observe des partitions de 50Mo séparées par le 3ème attribut. Peut-être est-ce un attribut plus pertinent que les 2 premiers pour notre partitionnement ?

### Qualité du partitionnement sur 5 attributs

* nbPlusieurs = 145250
* nbDepasses = 1

```
$ hdfs dfs -ls -h /user/p1513280/part5
Found 16 items
-rw-r--r--   3 p1513280 p1513280           0 2019-11-18 23:36 /user/p1513280/part5/_SUCCESS
-rw-r--r--   3 p1513280 p1513280      22.9 G 2019-11-18 23:36 /user/p1513280/part5/part_0_0_0_0_0
-rw-r--r--   3 p1513280 p1513280     115.8 M 2019-11-18 23:33 /user/p1513280/part5/part_0_0_0_0_1
-rw-r--r--   3 p1513280 p1513280     115.5 M 2019-11-18 23:33 /user/p1513280/part5/part_0_0_0_0_2
-rw-r--r--   3 p1513280 p1513280       4.5 K 2019-11-18 23:33 /user/p1513280/part5/part_0_1_0_1_0
-rw-r--r--   3 p1513280 p1513280         752 2019-11-18 23:33 /user/p1513280/part5/part_0_1_0_1_2
-rw-r--r--   3 p1513280 p1513280       1.5 K 2019-11-18 23:33 /user/p1513280/part5/part_1_0_1_0_0
-rw-r--r--   3 p1513280 p1513280         752 2019-11-18 23:33 /user/p1513280/part5/part_1_0_1_0_2
-rw-r--r--   3 p1513280 p1513280       3.7 K 2019-11-18 23:33 /user/p1513280/part5/part_1_1_1_1_0
-rw-r--r--   3 p1513280 p1513280       1.5 K 2019-11-18 23:33 /user/p1513280/part5/part_1_1_1_1_2
-rw-r--r--   3 p1513280 p1513280         759 2019-11-18 23:33 /user/p1513280/part5/part_1_2_1_2_1
-rw-r--r--   3 p1513280 p1513280         749 2019-11-18 23:33 /user/p1513280/part5/part_2_0_2_0_0
-rw-r--r--   3 p1513280 p1513280         758 2019-11-18 23:33 /user/p1513280/part5/part_2_0_2_0_2
-rw-r--r--   3 p1513280 p1513280         763 2019-11-18 23:33 /user/p1513280/part5/part_2_1_2_1_0
-rw-r--r--   3 p1513280 p1513280         727 2019-11-18 23:33 /user/p1513280/part5/part_2_1_2_1_2
-rw-r--r--   3 p1513280 p1513280         754 2019-11-18 23:33 /user/p1513280/part5/part_2_2_2_2_2
```

Légère séparation observée sur le 5ème attribut. On voit que le 3ème attribut plus autant les données.

### Qualité du partitionnement sur 10 attributs

* nbPlusieurs = 737257
* nbDepasses = 4

```
$ hdfs dfs -ls -h /user/p1513280/part10
Found 45 items
-rw-r--r--   3 p1513280 p1513280           0 2019-11-18 23:45 /user/p1513280/part10/_SUCCESS
-rw-r--r--   3 p1513280 p1513280      16.9 G 2019-11-18 23:45 /user/p1513280/part10/part_0_0_0_0_0_0_0_0_0_0
-rw-r--r--   3 p1513280 p1513280     424.6 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_0_0_0_1
-rw-r--r--   3 p1513280 p1513280      20.1 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_0_0_1_0
-rw-r--r--   3 p1513280 p1513280      20.2 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_0_0_1_1
-rw-r--r--   3 p1513280 p1513280      19.9 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_0_1_0_0
-rw-r--r--   3 p1513280 p1513280      20.2 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_0_1_0_1
-rw-r--r--   3 p1513280 p1513280      20.1 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_0_1_1_0
-rw-r--r--   3 p1513280 p1513280      20.0 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_0_1_1_1
-rw-r--r--   3 p1513280 p1513280      20.3 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_1_0_0_0
-rw-r--r--   3 p1513280 p1513280      20.2 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_1_0_0_1
-rw-r--r--   3 p1513280 p1513280      20.3 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_1_0_1_0
-rw-r--r--   3 p1513280 p1513280      20.1 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_1_0_1_1
-rw-r--r--   3 p1513280 p1513280      20.2 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_1_1_0_0
-rw-r--r--   3 p1513280 p1513280      20.0 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_1_1_0_1
-rw-r--r--   3 p1513280 p1513280      20.2 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_1_1_1_0
-rw-r--r--   3 p1513280 p1513280      20.0 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_0_1_1_1_1
-rw-r--r--   3 p1513280 p1513280       4.8 G 2019-11-18 23:43 /user/p1513280/part10/part_0_0_0_0_0_1_0_0_0_0
-rw-r--r--   3 p1513280 p1513280       8.0 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_0_0_0_1_0_0_0_1
-rw-r--r--   3 p1513280 p1513280     651.1 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_0_0_0_0
-rw-r--r--   3 p1513280 p1513280     114.4 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_0_0_0_1
-rw-r--r--   3 p1513280 p1513280       1.6 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_0_0_1_0
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_0_0_1_1
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_0_1_0_0
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_0_1_0_1
-rw-r--r--   3 p1513280 p1513280       1.6 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_0_1_1_0
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_0_1_1_1
-rw-r--r--   3 p1513280 p1513280       1.6 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_1_0_0_0
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_1_0_0_1
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_1_0_1_0
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_1_0_1_1
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_1_1_0_0
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_1_1_0_1
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_1_1_1_0
-rw-r--r--   3 p1513280 p1513280       1.5 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_0_1_1_1_1
-rw-r--r--   3 p1513280 p1513280      31.0 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_1_0_0_0_0
-rw-r--r--   3 p1513280 p1513280       4.9 M 2019-11-18 23:42 /user/p1513280/part10/part_0_0_1_0_0_1_0_0_0_1
-rw-r--r--   3 p1513280 p1513280         760 2019-11-18 23:42 /user/p1513280/part10/part_0_1_0_0_1_0_0_0_0_1
-rw-r--r--   3 p1513280 p1513280         753 2019-11-18 23:42 /user/p1513280/part10/part_0_1_0_0_1_0_0_1_1_1
-rw-r--r--   3 p1513280 p1513280         759 2019-11-18 23:42 /user/p1513280/part10/part_0_1_1_0_1_0_1_0_0_0
-rw-r--r--   3 p1513280 p1513280       1.5 K 2019-11-18 23:42 /user/p1513280/part10/part_1_0_0_1_0_0_0_0_0_1
-rw-r--r--   3 p1513280 p1513280         749 2019-11-18 23:42 /user/p1513280/part10/part_1_0_1_1_0_0_0_0_1_0
-rw-r--r--   3 p1513280 p1513280         758 2019-11-18 23:42 /user/p1513280/part10/part_1_0_1_1_0_0_1_0_1_0
-rw-r--r--   3 p1513280 p1513280         754 2019-11-18 23:42 /user/p1513280/part10/part_1_1_1_1_1_0_1_0_0_0
-rw-r--r--   3 p1513280 p1513280         727 2019-11-18 23:42 /user/p1513280/part10/part_1_1_1_1_1_0_1_1_1_1
```

Ici on commence à observer un partitionnement un peu plus prononcé. 

### Atributs utilisés pour le partitionnement 

Voici les 10 attributs utilisés dans le partitionnement avec 10 attributs. Ce sont donc les 10 attributs avec les plus petits écart-types moyens non nuls.
L'affichage est de la forme `(index de l'attribut, minimum, taille des intervalles de partitionnement)` et correspond à la variable `nBest` dans notre code.

* (7,0.0,51.293)
* (10,0.0,48.4891)
* (26,3.38284E-4,1072494.999830858)
* (30,0.0,51.293)
* (32,0.0,48.4891)
* (51,0.0,0.5)
* (52,4.27232E-5,4.400484999997864E7)
* (54,2.01548E-5,1.4165899999989923E7)
* (56,-7673040.0,4.291607E7)
* (84,0.00173927,1.69653E38)


### Conclusion sur le partitionnement

Il semble en effet indispensable de faire varier la taille des intervalles utilisés pour le partitionnement en prenant en compte la densité des zones des valeurs prises par les attributs.

On pourra également enlever certains fractiles en tête et queue des valeurs de certains attributs (les outliers) pour éviter des étirements des intervalles. Ces valeurs extrêmes seraient alors mises dans le premier et le dernier intervalle.